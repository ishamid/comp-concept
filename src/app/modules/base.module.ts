import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BaseRoutingModule } from './base-routing.module';
import { HomepageComponent } from './homepage/homepage.component';

@NgModule({
  declarations: [
    HomepageComponent
  ],
  imports: [
    BaseRoutingModule
  ],
  providers: [],
})

export class BaseModule { }
